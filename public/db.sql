CREATE TABLE application (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  name varchar(128) NOT NULL DEFAULT '',
  mail varchar(128) NOT NULL DEFAULT '',
  year int(4) NOT NULL ,
  pol text(4) NOT NULL DEFAULT '',
  countlimbs int(8) NOT NULL DEFAULT 0,
  super text(128) NOT NULL ,
  biography text(128) NOT NULL ,
  check1 text(8) NOT NULL DEFAULT 'no',
  PRIMARY KEY (id)
);
